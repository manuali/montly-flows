# Progetto Monthly Flows

## Table of Contents

1. [Analisi progetto](#analisi)<br>
2. [Da Realizzare](#da-realizzare)<br>
[*Documento Aggiornamento produzione*](#documento-aggiornamento-produzione)<br>
[*Documento Parametri Broker*](#documento-parametri-broker)

## Analisi

#### Esigenze

La Wifed srl ha la necessità di analizzare i flussi di cassa (attivi e passivi) e la produzione ogni mese. Per raggiungere questo obiettivo è necessario reperire le seguenti informazioni:

## Tipologia di entità

**Entrate** <br> Le Entrate Genereranno dei flussi di cassa positivi di diversi tipi (in base alla loro origine)

- **Previsionali da wheels (codice: PW)**  <br>
Questa classe di entrate conterrà tutte le entrate che come origine hanno una prenotazione effettuata su wheels. Tramite opportune analisi, da esse, verrà generata un'entrata alla data che il programma definirà in base al broker definito nella prenotazione. A tal fine dovrà essere realizzato su mago un form in cui potremo censire i broker e definire i periodi con cui accreditano alla winfed gli importi dei pagamenti delle carte di credito. Ogni entrata previsionale da wheels avrà uno stato "pending" (ancora non è stato generato un noleggio) o "consuntivata" (esiste un contratto di noleggio).

- **Consuntivate da mago (CODICE: CM)** <br>
Questa classe di entrate avrà come origine il partitario attivo di mago da cui verranno selezionate le partite che hanno scadenza nel mese ma non fanno riferimento a noleggi (tutte le partite relative a noleggi hanno come nr documento il nr di contratto e quindi sono già statre inserite nel bilancio del mese come entrate). Quindi saranno presenti tutte le partite il cui riferimento non è un contratto di noleggio.

---

**Uscite** <br> Le Uscite genereranno flussi di cassa negativi di diversi tipi (in base alla loro origine)

- **Consuntivate da mago (CODICE: CM)** <br>
Sono le rate in scadenza nel mese originate dalle partite di mago relative ai noleggi e alle fatture passive registrate su mago. Al fine di classificare meglio la caratteristica dell'uscita sarebbe interessante associare il conto di costo del piano dei conti imputabile nella contropartita di costo in anagrafica fornitore di Mago.

- **Previsionali (CODICE: UP)** <br>
 Verranno immesse manualmente e saranno relative a previsioni di uscita libere.

---

## Rappresentazione dei dati

Al fine di gestire tutte le informazioni verrà creato un documento di mago **Monthly Flows** che conterrà i dati e delle funzioni di aggiornamento che consentiranno di avere una base dati sempre aggiornata. Tale Base Dati potrà essere utilizzata per produrre report è essere interrogata direttamente da excel

|Tipo|Origine|Classe|Cliente - fornitore|Importo prev|Di cui Imposta|Data|Stato|Broker|IdOrigine|
|---|---|---|---|---|---|---|---|---|---|
|Entrata|PW|PP||14.00||30/10/2019|pending|CARTRAWLER|1200
|Entrata|PW|POA||140.00||30/10/2019|pending|CARTRAWLER|1300
|Entrata|PW|EXTRA||60.00||30/10/2019|consolidata|---|1400
|Uscita|CM|Leaseplane|00464|60.00||30/10/2019|consolidata|---|1500

- I campi 'Tipo' e 'Origine' contengono il tipo di operazione e il codice dell'orgine. 
- Il campo classe contiene un velore che servira a categorizzare l'entrata o l'uscita e potrà essere usato come eventuale raggruppamento. Dovrà essere immesso manualmente per categorizzare le tipologie di uscita (in attesa di individuare un modo per automatizzare la sua compilazione). In caso di entrate, in base al foglio prodotto da wheels saremo in grado di effettuare una classificazione (es: Pay on Arrival o Pre Payed).
- Il Campo Cliente - Fornitore conterrà il codice del fornitore contenuto nella partita (sarà presente solo per le uscite) e potrà essere usato come criterio di raggruppamento per eventuale reportistica.
- Il Campo Importo Prev conterrà il valore delle entrate e delle uscite con natura previsionale (codici: PW | UP)
- Il campo 'Di cui imposta' conterrà , ove possibile, l'importo dell'Iva relativa all'entrata o all'uscita. Potrà essere utilizzato ai fini della reportistica.
- Il Campo 'Data' conterrà la data dell'entrata o dell'uscita.
- IL Campo 'Stato' verrà aggiornato in base allo stato dell'uscita (pending se non presente il contratto di noleggio)
- Il Campo Broker servira per calcolare la data esatta dell'entrata. Nel caso di extra maturati alla chiusura del contratto verrà generata una entrata alla data della chiusura
- Il Campo IdOrigine conterrà il riferimento alla riga del report di produzione generato da wheels (nel caso di entrate) e alla riga partita di mago.

## Da Realizzare

### Su Mago 4

1. Documento **Aggiornamento Produzione**<br> 
Si dovrà realizzare su mago 4 un documento che importi periodicamente i dati della produzione da wheels. Esso dovrà essere eseguito per mese in modo da consentire sia l'acquisizione di prenotazioni nuove sia l'aggiornamento di prenotazioni che sono diventate contratti. Sarà un documento batch di mago che consentirà di importare il file di excel prodotto da wheels. La sua funzione di Aggiornamento consisterà nel creare un archivio interno a mago che servirà a generare Il Cash Flow Mensile. Se, per esempio, in data 15/09/2019 volessimo Aggiornare la produzione del mese di Ottobre ci troveremo nel documento Cash Flow di ottobre tutte entrate di tipo PW in stato pending. Gli ulteriori aggiornamenti del mese (generati dalle nuove importazioni del foglio excel di wheels) aggiorneranno lo stato delle entrate da pending a consolidato o creeranno nuove entrate di tipo PW per le prenotazioni nuove.

2. Documento **Monthly Flows**<br> 
Questo documento conterrà una funzione di aggiornamento che provvederà a leggere i dati provenienti dall'aggiornamento produzione e genererà la struttura illustrata in [rappresentazione dati](#rappresentazione-dei-dati) (che chiaramente in questo documento è descritta nela sua fase embrionale e sarà sicuramente oggetto di revisioni in base alle esigenze rinvenute sul campo). L'obiettivo di questo documento è dare per ogni giorno del mese l'importo relativo all'ammontare delle entrate e delle uscite. Tramite questo documento, sarà possibile realizzare report su mago o estrazione dirette su fogli excel.

3. Documento **Parametri Broker**<br>
Questo documento consentira di definire le condizioni dei broker (date di accredito) o altri dati da definire

4. Documento **Tabella Classi**<br>
Questo documento consentira di definire le classi da impostare nelle righe del documento Monthly Flows per categorizzare le voci di entrata e di uscita

# Documento Aggiornamento produzione 

Da wheels viene prodotto (ogni volta che sarà necessario il foglio excel su base mensile che contiene la produzione del mese) come da [foglio](./produzione.xlsx) excel. Le colonne del foglio che andranno importate sono le seguenti:

|Nome Colonna|Descrizione|tipo campo|
|---|---|---|
|Res no|Nr. di prenotazione su wheels|varchar(50)
|Check-in Date|Data inizio noleggio|DateTime
|Check-out Date|Data fine noleggio|DateTime
|Days|Giorni noleggio|int
|Check-out Station|Stazione di riferimento (centro di costo di mago)|Varchar (50)
|Charged group|Gruppo|Varchar(12)
|Vehicle|Targa veicolo|Varchar(50)
|Agr. no|Nr. Contratto di noleggio|Varchar(50)
|Booking date|Data prenotazione|DateTime
|Group|Gruppo|Varchar(12)
|Rate per day|Rate per giorno|Double
|Agent Taxid|Partita iva da usare come codice broker nella tabella|Varchar(30)
|Total|Totale noleggio|Double
|Paid|Importo pagato|Double
|Balance|Saldo tra Totale e pagato|Double
|Rental charge|?|Double
|Net|?|Double
|VAT|Importo iva|Double
|Grand Total|?|Double
|Pre-paid amount|Importo pre-pagato server a distinguere la classe di entratata tra pre-payed e Pay on arrival| Double

#### Struttura DB documento

---

Tabella di testa

|Nome Tabella|Descrizione|
|---|---|
|Tag_MFL_WheelsProdTeste|Tabella contenente le teste dei documenti mensili di aggiornamento prroduzione da wheels|

|Nome Colonna|Descrizione|tipo campo|
|---|---|---|
|Mese|Mese di riferimento|int (***PK***)
|Anno|Mese di riferimento|int (***PK***)
|LastUpdate|Data ultimo aggiornamento|DateTime
---
Tabella righe

|Nome Tabella|Descrizione
|---|---
|Tag_MFL_WheelsProdRighe|Tabella contenente le righe dei documenti mensili di aggiornamento produzione da wheels

|Nome Colonna|Descrizione|tipo campo|
|---|---|---|
|Mese|Mese di riferimento|int (***PK***)
|Anno|Mese di riferimento|int (***PK***)
|Riga|Progressivo di riga|int (***PK***)
|LastUpdate|Data ultimo aggiornamento|DateTime
|Res no|Nr. di prenotazione su wheels|varchar(50)
|Check-in Date|Data inizio noleggio|DateTime
|Check-out Date|Data fine noleggio|DateTime
|Days|Giorni noleggio|int
|Check-out Station|Stazione di riferimento (centro di costo di mago)|Varchar (50)
|Charged group|Gruppo|Varchar(12)
|Vehicle|Targa veicolo|Varchar(50)
|Agr. no|Nr. Contratto di noleggio|Varchar(50)
|Booking date|Data prenotazione|DateTime
|Group|Gruppo|Varchar(12)
|Rate per day|Rate per giorno|Double
|Agent Taxid|Partita iva da usare come codice broker nella tabella|Varchar(30)
|Total|Totale noleggio|Double
|Paid|Importo pagato|Double
|Balance|Saldo tra Totale e pagato|Double
|Rental charge|?|Double
|Net|?|Double
|VAT|Importo iva|Double
|Grand Total|?|Double
|Pre-paid amount|Importo pre-pagato server a distinguere la classe di entratata tra pre-payed e Pay on arrival| Double

---

# Documento Parametri Broker

Documento di mago 4 in cui censire i broker e definire le modalità di accredito

#### Struttura DB documento

---

Tabella di testa

|Nome Tabella|Descrizione|
|---|---|
|Tag_MFL_WheelsBrokersData|Tabella contenente le anagrafiche dei broker|

|Nome Colonna|Descrizione|tipo campo|
|---|---|---|
|IdBroker|Partita Iva del broker (presente nelle righe del foglio excel di produzione)|Varchar(50)(***PK***)
|RagSoc|Ragione sociale del broker|int (***PK***)
|Tipo|Enumerativo sul tipo di calcolo da effettuare da prevedere per ora solo un valore 001 (mese successivo alla data di check out) vedremo in seguito se esitono altri tipi di algoritmi da gestire|Int
|GiornoAcc| Giorno del mese in cui viene fatto accredito (vale se tipo = 001)|int
|BaseCalcolo|Enumerativo che identifica la data di partenza per eseguire il calcolo (es. 001 Data Check out fine mese o 002 Data Ceck out esatta)|int


---
