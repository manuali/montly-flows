# Note Operative Infobusiness

1.[Report Bilancio Previsionale](#report-bilancio-previsionale)

## Report Bilancio Previsionale

l'obbiettivo del report è quello di potere, tramite il modelling, inserire le previsioni dei costi e ricavi e confrontarle con i consuntivi del periodo.

**Prerequisiti**

per potere funzionare è necessario inizializzare i saldi dei conti economici per farlo eseguire i seguenti step:

### Creare la seguente stored procedure nel db dell'azienda

~~~sql
USE [WinfedSrl_2018_Mago4]
GO

/****** Object:  StoredProcedure [dbo].[tag_sp_creamodello]    Script Date: 25/10/2019 12:38:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[tag_sp_creamodello]
--parametri in input
@modello varchar(8),
@sezione varchar(16)
AS
DECLARE
@template varchar(8),
@line int,
@Account varchar(16),
@DebitCreditSign int,
@DebitCreditSignContropartita int

IF @sezione = '06' 
set @DebitCreditSign = 4980736 
set @DebitCreditSignContropartita = 4980737;

IF @sezione = '07' 
set @DebitCreditSign = 4980737 
set @DebitCreditSignContropartita = 4980736;

set @line = 0;
set @template = @modello;


DECLARE Costi_cursor CURSOR FOR
SELECT [Account]
  FROM [WinfedSrl_2018_Mago4].[dbo].[MA_ChartOfAccounts]
  where Ledger = @sezione and LEN(Account) = 8
  order by account
OPEN Costi_cursor
FETCH NEXT FROM Costi_cursor
into @Account

WHILE @@FETCH_STATUS = 0

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	set @line = @line +1;

	-- PRINT @Account + ' - ' + CAST(@line AS varchar);

	INSERT INTO [dbo].[MA_AccTemplatesGLDetail]
           ([Template]
           ,[Line]
           ,[AccRsn]
           ,[AccountType]
           ,[CustSupp]
           ,[Account]
           ,[AmountType]
           ,[DebitCreditSign]
           ,[Amount]
           ,[Automatic]
           ,[TaxBalancingCheck]
           ,[Notes]
           ,[IgnoreSign]
           ,[OffsetGroupNo]
           ,[AutoBalance]
           ,[TBCreated]
           ,[TBModified]
           ,[TBCreatedID]
           ,[TBModifiedID])
     VALUES
           ( @template,
             @line,
            'GIROCONT',
			3080194,
			'',
            @Account,
            6356992,
			@DebitCreditSign,
			1,
           '0',
		   '0',
           'note',
           '0',
            0,
           '0',
           GETDATE (),
           GETDATE (),
           2,
           2)

	FETCH NEXT FROM Costi_cursor INTO @Account;
	

END;


-- INSERISCO L'ULTIMA RIGA DEL MODELLO
SET @line = @line +1;

INSERT INTO [dbo].[MA_AccTemplatesGLDetail]
           ([Template]
           ,[Line]
           ,[AccRsn]
           ,[AccountType]
           ,[CustSupp]
           ,[Account]
           ,[AmountType]
           ,[DebitCreditSign]
           ,[Amount]
           ,[Automatic]
           ,[TaxBalancingCheck]
           ,[Notes]
           ,[IgnoreSign]
           ,[OffsetGroupNo]
           ,[AutoBalance]
           ,[TBCreated]
           ,[TBModified]
           ,[TBCreatedID]
           ,[TBModifiedID])
     VALUES
           ( @template,
             @line,
            'GIROCONT',
			3080194,
			'',
           '08021001',
            6356992,
			@DebitCreditSignContropartita,
			@line - 1,
           '0',
		   '0',
           'note',
           '0',
            0,
           '0',
           GETDATE (),
           GETDATE (),
           2,
           2)


CLOSE Costi_cursor;
DEALLOCATE Costi_cursor;

GO
~~~

### Creare i modelli contabili

la procedura aggiunge le righe rispettivamente della sezione COSTI come corpo della causale scelta per inizializzare rispettivamente costi e ricavi.

***Per creare ed aggiornare il modello contabile di mago che inizializza i costi :***

Eseguire le seguenti istruzioni:
~~~sql
-- svuoto il corpo della causale 
DELETE FROM [MA_AccTemplatesGLDetail] WHERE Template = 'INITCOST'
-- lo riempio con tutta la sezione dei costi
EXEC [dbo].[t***ag_sp_creamodello] 'INITCOST', '06'
~~~

***Per creare ed aggiornare il modello contabile di mago che inizializza i ricavi :***

Eseguire le seguenti istruzioni:
~~~sql
-- svuoto il corpo della causale 
DELETE FROM [MA_AccTemplatesGLDetail] WHERE Template = 'INITRIC'
-- lo riempio con tutta la sezione dei costi
EXEC [dbo].[t***ag_sp_creamodello] 'INITRIC', '07'
~~~

### Immettere su mago4 i movimenti di inizializzazione

Per inizializzare i saldi andra' fatto per ogni mese dell'anno un movimento (il primo del mese) con le causuali **INITCOST** e **INITRIC**

## Procedura alternativa 

Fatte la registrazioni la prima volta è più semplice aggiornare le causali "INITCOST" e "INITRIC" inserendo (prima di effettuare le registrazioni di ogni mese come descritto sopra) solo i conti che sono presenti nel piano dei conti (i conti nuovi immessi dopo) che non seono presenti tra le righe dei costi e ricavi dei due modelli contabili.

A tale scopo vanno effettuate le seguenti query che ci mostrano questi dati:
### per i costi
~~~sql
select 
a.Account,
a.Description,
b.Account as contomodello
from MA_ChartOfAccounts a 
left join MA_AccTemplatesGLDetail b on a.Account = b.Account and b.Template = 'INITCOST'
where a.Ledger = '06' and len(a.Account) = 8 and b.Account  is NULL
~~~
### per i ricavi
~~~sql
select 
a.Account,
a.Description,
b.Account as contomodello
from MA_ChartOfAccounts a 
left join MA_AccTemplatesGLDetail b on a.Account = b.Account and b.Template = 'INITRIC'
where a.Ledger = '07' and len(a.Account) = 8 and b.Account  is NULL
~~~

Aggiornare anche l'ultima riga dei due modelli che giroconta il totale (ogni riga = 1 euro)


